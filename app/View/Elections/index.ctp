<?php echo $this->Session->flash('addelection')?>
<div class="CreateElectionForm">  
    <h3>Create New Election</h3>
<?php 
    echo $this->Form->create('Elections',array('class' => 'form-horizontal',
                                        'action' => 'add',
                                        'inputDefaults' => array(
                                            'format' => array('before', 'label', 'between','error','input','after'),
                                            'div' => array('class' => 'form-group'),
                                            'between' => '<div class="col-sm-9">',
                                            'after' =>'</div>',
                                            'label' => array('class' => 'col-sm-3 control-label'),
                                            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                                            'class' => 'form-control'
                                        )));
    echo $this->Form->input('title');
    echo $this->Form->input('desc',array('label'=>array('text' => 'Descrption','class' => 'col-sm-3 control-label')));   
    echo $this->Form->end(array('label' => 'Add New','class'=>'btn btn-default'));
?>
</div>
<table class ="table">
    <tr><th>Title</th><th>Created On</th><th>Actions</th><th>Passcode*</th>
    <?php       
    foreach($elections_list as $elec){
        ?>
    <tr>
        <?php
        echo '<td>'.$elec['Elections']['title'].'</td>';
        echo '<td>'.$this->Time->nice($elec['Elections']['createdOn']).'</td>';
        ?>
        <td><?php if($elec['Elections']['mode'] == "editing"){?>
            <a class="btn btn-default" href="<?php echo $this->Html->url(array('controller' => 'elections','action' => 'edit',$elec['Elections']['id']))?>">Edit</a>
            <?php } ?>
            <?php if($elec['Elections']['mode'] != "open" && $elec['Elections']['mode'] != "over"){?>
                <a class="btn btn-default" onclick='return confirm("Once you have open the elecion you will not be able to edit the election, confirm?");' href="<?php echo $this->Html->url(array('controller' => 'elections','action' => 'open',$elec['Elections']['id']))?>">Open</a>
            <?php } ?>
            <?php if($elec['Elections']['mode'] == "open" && $elec['Elections']['mode'] != "over"){?>
                <a class="btn btn-default" onclick='return confirm("Are you sure you want to close the election?, you will be able to open it again until election is finished");' href="<?php echo $this->Html->url(array('controller' => 'elections','action' => 'close',$elec['Elections']['id']))?>">Close</a>
            <?php } ?>
            <?php if($elec['Elections']['mode'] != "editing" && $elec['Elections']['mode'] != "over"){?>
                <a class="btn btn-default" onclick='return confirm("Are you sure you want to finish the election?, you will not be able to open it again and results will be produced");' href="<?php echo $this->Html->url(array('controller' => 'elections','action' => 'over',$elec['Elections']['id']))?>">Finish</a>
            <?php } ?>
            <?php if($elec['Elections']['mode'] != "over"){?>
            <a class="btn btn-default" href="<?php echo $this->Html->url(array('controller' => 'voters','action' => 'manage',$elec['Elections']['id']))?>">Manage Voters</a>
            <?php } ?>
            <?php if($elec['Elections']['mode'] == "over"){?>
            <a class="btn btn-default" href="<?php echo $this->Html->url(array('controller' => 'elections','action' => 'results',$elec['Elections']['id']))?>">View Results</a>
            <?php } ?>
        </td>
        <td>
            <?php if($elec['Elections']['mode'] != "over"){ ?>
                <?php if($elec['Elections']['passcode'] == 0){
                    echo "Not Set &nbsp;&nbsp;";
                    }
                    else{
                        echo $elec['Elections']['passcode'];
                        echo '&nbsp;&nbsp;';
                    }
                    ?>
                <a class="btn btn-default" href="<?php echo $this->Html->url(array('controller' => 'elections','action' => 'passcode',$elec['Elections']['id'],'generate'))?>">Generate</a>
                <a class="btn btn-default" href="<?php echo $this->Html->url(array('controller' => 'elections','action' => 'passcode',$elec['Elections']['id'],'remove'))?>">Remove</a>
            <?php } ?>
        </td>
    </tr>
        <?php
    }
    
    ?>
</table>
<div class="pagination pagination-large">
    <ul class="pagination">
        <?php
            echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
            echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
        ?>
    </ul>
</div>
<h5>*passcode - passcode is used to provide extra layer of security to the election page, this passcode will be used to log into election page</h5>
