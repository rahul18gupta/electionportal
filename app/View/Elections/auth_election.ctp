<h4>Provide Passcode for the election</h4>
<div class="row">
	<form method="post" class="col-sm-4">
		<div class="form-group">
	    	<label for="passcode">Passcode</label>
	    	<input type="password" class="form-control" id="passcode" name="data[passcode]" placeholder="Enter Passcode">
	  	</div>
	  	<button class="btn btn-default" type="submit">Submit</button>

	</form>
</div>