<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Voters
 *
 * @author rahul
 */
App::uses('AppModel', 'Model');

class Voters extends AppModel{
    //put your code here
    public $tablePrefix = 'portal_';
    public $_schema = array(
        'voter_id' => array(
            'type' => 'string',
            'length' => 15,            
        ),
        'is_voted' => array(
            'type' => 'boolean'            
        ),        
        'secret' => array(
            'type' => 'string',
            'length' => 10,
        ),        
        'details' => array(
            'type' => 'text'            
        ),
        'election_id' => array(
            'type' => 'integer',
            'length' => 10,
        )
    );
    public $belongsTo = array (
        'Election' => array(
            'className' => 'Elections',
            'foreignKey' => 'election_id'
        )
    );   
            
}
