<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

class Users extends AppModel{
    public $name = 'Users';
    public $tablePrefix = 'portal_';
    public $_schema = array(
        'id' => array(
            'type' => 'integer',
            'length' => 10,            
        ),       
        'ldapId' => array(
            'type' => 'string',
            'length' => 30
        ),
        'level' => array(
            'type' => 'integer',
            'length' => 1
        )       
    ); 
    public $primaryKey = 'id';
    public $displayField = 'ldapId';
    public $validate = array(
        'ldapId' => array(
            'required' => true,
            'rule' => 'isUnique',
            'message' => 'ldap id already exists'
        ),
        'level' => array(
            'required' => true,
            'rule'    => array('inList', array(1, 2)),
            'message' => 'Level can be either 1 or 2'
        )
    );
}