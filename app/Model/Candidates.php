<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Candidates
 *
 * @author rahul
 */

App::uses('AppModel', 'Model');

class Candidates extends AppModel{
    //put your code here
    public $tablePrefix = 'portal_';
    public $_schema = array(
        'id' => array(
            'type' => 'integer',
            'length' => 10,            
        ),
        'name' => array(
            'type' => 'string',
            'length' => 30
        ),        
        'post_id' => array(
            'type' => 'integer',
            'length' => 10,
        ),
        'election_id' => array(
            'type' => 'integer',
            'length' => 10,
        ),
        'image_path' => array(
            'type' => 'text',            
        ),
        'votes_yes' => array(
            'type' => 'integer',
            'length' => 10
        ),
        'votes_no' => array(
            'type' => 'integer',
            'length' => 10
        ),
        'votes_neutral' => array(
            'type' => 'integer',
            'length' => 10
        ),
        'order' => array(
            'type' => 'integer'          
        )
    );
    public $belongsTo = array (
        'Election' => array(
            'className' => 'Elections',
            'foreignKey' => 'election_id'
        ),
        'Post' => array(
            'className' => 'Posts',
            'foreignKey' => 'post_id'            
        )
    );
    
    public $primaryKey = 'id';
    public $validate = array(
        'name' => array(
            'alphaNumeric' => array(
                'rule'     => array('custom', '/^[a-z0-9 ]*$/i'),
                'required' => true,
                'message'  => 'Alphabets only'
            ),
            'length' => array(
                'rule' => array('maxlength',30),
                'message' => 'max length allowed is 30'
            )
        )        
    );
}
