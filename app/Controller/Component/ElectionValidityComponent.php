<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CheckValidityComponent
 *
 * @author rahul
 */
App::uses('Component', 'Controller');
class ElectionValidityComponent extends Component{
    public $components = array('Auth');
    public $uses = array('Elections');       
    
    protected function checkEditibility_array($election){
        $user = $this->Auth->user();
        if($election['Elections']['mode'] != "editing")
            throw new ForbiddenException("election no longer is editable"); 
        if($election['Elections']['admin_id'] != $user['Users']['id'])
            throw new ForbiddenException("Your are not allowed to edit this election");
    }
    
    public function checkEditibility($election){
        if(is_array($election)){
            $this->checkEditibility_array($election);
        }
        else{          
            $election_model = ClassRegistry::init('Elections');
            $election_model->recursive = 0;
            $data = $election_model->findByid($election);            
            if(!$data)
                throw new NotFoundException("Corresponding election $election doesn't exists");
            $this->checkEditibility_array($data);
        }        
    }

    public function checkUser($election){
        $user = $this->Auth->user();        
        if($election['Elections']['admin_id'] != $user['Users']['id'])
            throw new ForbiddenException("Your are not allowed to edit this election");   
    }
    //put your code here
}
