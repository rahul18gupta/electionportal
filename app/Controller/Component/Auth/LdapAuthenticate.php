<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LdapAuthenticate
 *
 * @author rahul
 */
App::uses('BaseAuthenticate', 'Controller/Component/Auth');

class LdapAuthenticate extends BaseAuthenticate {
    protected function ldapauth($userid, $userpass) {
        $ldap_uid = mysql_escape_string($userid);
        $ldap_pass = mysql_escape_string($userpass);
        $ds = ldap_connect("ldap.iitb.ac.in");
        if (!$ds) {
            throw new InternalErrorException("Unable to connect to LDAP server. Please try again later.");
        }
        $sr = ldap_search($ds, "dc=iitb,dc=ac,dc=in", "(uid=$ldap_uid)");
        $info = ldap_get_entries($ds, $sr);
        $ldap_uid = $info[0]['dn'];
        $do_bind = @ldap_bind($ds, $ldap_uid, $ldap_pass);
        if($do_bind){
            //return strtolower($info[0]['employeenumber'][0]);
            return true;
        }else{
            return false;
        }           
    }

    public function authenticate(CakeRequest $request, CakeResponse $response) {
        $userModel = $this->settings['userModel'];        
        $user = ClassRegistry::init($userModel)->find('first',array('conditions'=> array('Users.ldapId' => $request->data['users']['username'])));
        if($user){
            if ($this->ldapauth($request->data['users']['username'], $request->data['users']['password'])) {
                return $user;
            } else {
                return false;
            }
        }
        else{
            return false;
        }
        // Return an array of user if they could authenticate the user,
        // return false if not
    }
}
