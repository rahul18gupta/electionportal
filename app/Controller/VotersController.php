<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VotersController
 *
 * @author rahul
 */
App::uses('AppController', 'Controller');

class VotersController extends AppController{     
	public $uses = array('Elections','Voters');
    public $helpers = array('Paginator','Html');
    public $scaffold = 'admin';
    public $components = array('ElectionValidity');	

    public function manage($id){
        $this->Elections->unbindModelAll();
        $elec = $this->Elections->findByid($id);
        if($elec == null || $elec['Elections']['mode'] == 'over'){
            throw new NotFoundException();
        }
        $this->ElectionValidity->checkUser($elec);        

        $input_valid = true;
        $success_save = true;        
        if($this->request->is('post') && isset($this->request->data['Upload'])){
            $data = $this->request->data['Upload'];
            $field_no = intval($data['column_no']);
            if($field_no > 0 && isset($data['file'])){
                $file = fopen($data['file']['tmp_name'],"r");
                if(isset($data['ignore']))
                    fgetcsv($file);    
                $this->Voters->unbindModelAll(true);   
                $datasource = $this->Voters->getDataSource();
                $datasource->begin();                
                while(!feof($file))
                {
                    $voter_info = fgetcsv($file);
                    if(!isset($voter_info[$field_no - 1])){
                        continue;                        
                    }
                    $rollnumber = strtolower(trim($voter_info[$field_no - 1]));
                    if(strlen($rollnumber) > 10){
                        $input_valid = false;break;
                    }
                    $voter = $this->Voters->find('first',array('conditions' => array('voter_id' =>$rollnumber, 'election_id' => $id)));
                    if(count($voter) > 0 && !$voter['Voters']['is_voted']){
                        $this->Voters->deleteAll(array('voter_id' =>$rollnumber, 'election_id' => $id),false);      
                    }
                    if(count($voter) == 0 || !$voter['Voters']['is_voted']){                    
                        if(!$this->Voters->save(array('voter_id' =>$rollnumber, 'election_id' => $id,'details' => json_encode($voter_info),'secret' => rand(100000,999999)))){
                            $success_save = false;break;
                        }
                    }
                }
                if($success_save && $input_valid){
                    $datasource->commit();       
                    $this->Session->setFlash("Successfully updated voters list, please download the new list which contains the secret key for voters",'success_flash',array());
                }
                else     
                    $datasource->rollback();       
            }
            else{
                $input_valid = false;        
            }
        }
        if(!$input_valid)
            $this->Session->setFlash("Please provide correct input",'error_flash',array());
        if(!$success_save)
            $this->Session->setFlash("Some error occured, try again",'error_flash',array());
        $this->set('election',$elec);     
        $this->layout = "main";
        $this->set('title_for_layout',"manage voters list");
    }

    public function download(){
        if($this->request->is('post')){
            $election_id = $this->request->data['Download']['election_id'];
            $elec = $this->Elections->findByid($election_id);
            if($elec == null || $elec['Elections']['mode'] == 'over'){
                throw new NotFoundException();
            }
            $this->ElectionValidity->checkUser($elec);
            $match_string = "";
            if(isset($this->request->data['Download']['match_string']))
                $match_string = $this->request->data['Download']['match_string'];
            $this->Voters->unbindModelAll();
            $voters = $this->Voters->find('all',array('conditions' => array('Voters.details LIKE' => '%'.$match_string.'%','Voters.election_id' => $election_id )));            
            //var_dump($voters);
            $path = 'php://temp';
            $fp = fopen($path, 'r+');
            foreach ($voters as $value) {
                $voter_info = json_decode($value['Voters']['details']);
                $voter_info[] = $value['Voters']['secret'];                
                fputcsv($fp,$voter_info);                
            }
            rewind($fp);
            $output = fread($fp, 1048576);
            fclose($fp);                                    
            $this->response->body($output);
            $this->response->type('text/csv');
            $this->response->download('voters_list_'.$match_string.'.csv');                        
            return $this->response;            
        }
        else{
            throw new BadRequestException();
        }
        exit();        
    }

    public function delete(){
        if($this->request->is('post')){
            $election_id = $this->request->data['Delete']['election_id'];
            $elec = $this->Elections->findByid($election_id);
            if($elec == null || $elec['Elections']['mode'] == 'over'){
                throw new NotFoundException();
            }
            $this->ElectionValidity->checkUser($elec);
            $match_string = "";
            if(isset($this->request->data['Delete']['match_string']))
                $match_string = $this->request->data['Delete']['match_string'];
            $this->Voters->unbindModelAll();
            if($this->Voters->deleteAll(array('details LIKE' => '%'.$match_string.'%','election_id' => $election_id ,'is_voted' => false ),false)){
                $this->Session->setFlash("Successfully deleted the voters list",'success_flash',array());
            }else{
                $this->Session->setFlash("Some error occured while deleting please try again",'error_flash',array());
            }        
            
        }
        else{
            throw new BadRequestException();
        }
        $this->redirect(array('action' => 'manage',$election_id));
    }
}
?>